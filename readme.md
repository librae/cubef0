# STM32Cube Package Build

## Issues

### Windows Paths

```sh
In file included from ../../../../Drivers/BSP/Components/l3gd20/l3gd20.c:39:0:
../../../../Drivers/BSP/Components/l3gd20/l3gd20.h:48:28: fatal error: ..\Common\gyro.h: No such file or directory
 #include "..\Common\gyro.h"

In file included from ../../../../Drivers/BSP/STM32F072B-Discovery/stm32f072b_discovery_gyroscope.c:39:0:
../../../../Drivers/BSP/STM32F072B-Discovery/stm32f072b_discovery_gyroscope.h:51:44: fatal error: ..\Components\l3gd20\l3gd20.h: No such file or directory
 #include "..\Components\l3gd20\l3gd20.h"
```

## GDB Usage

1. Start the GDB server.

```
$ st-util
2014-11-27T15:48:19 INFO src/stlink-common.c: Loading device parameters....
2014-11-27T15:48:19 INFO src/stlink-common.c: Device connected is: F4 device (Dynamic Efficency), id 0x10006433
2014-11-27T15:48:19 INFO src/stlink-common.c: SRAM size: 0x18000 bytes (96 KiB), Flash: 0x80000 bytes (512 KiB) in pages of 16384 bytes
Chip ID is 00000433, Core ID is  2ba01477.
Target voltage is 3251 mV.
Listening at *:4242...
```

2. Turn to another console, connect as client:

```
$ arm-none-eabi-gdb build/cube.elf
GNU gdb (GNU Tools for ARM Embedded Processors) 7.6.0.20131129-cvs
Copyright (C) 2013 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
and "show warranty" for details.
This GDB was configured as "--host=i686-linux-gnu --target=arm-none-eabi".
For bug reporting instructions, please see:
<http://www.gnu.org/software/gdb/bugs/>...
Reading symbols from /home/yrliao/Documents/arm/STM32Cube_FW_F4_V1.3.0/Projects/STM32F401RE-Nucleo/Examples/UART/UART_Printf/GCC/build/cube.elf...done.
(gdb) tar ext :4242
Remote debugging using :4242
Reset_Handler () at startup_stm32f401xe.S:80
80	  ldr   sp, =_estack    		 /* set stack pointer */
(gdb) continue
Continuing.
^C
Program received signal SIGTRAP, Trace/breakpoint trap.
0x08000716 in HAL_GetTick () at ../../../../../../Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal.c:318
318	  return uwTick;
(gdb) bt
#0  0x08000716 in HAL_GetTick () at ../../../../../../Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal.c:318
#1  0x0800072e in HAL_Delay (Delay=Delay@entry=1000)
    at ../../../../../../Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal.c:336
#2  0x08006280 in main () at ../Src/main.c:118
(gdb) kill
Kill the program being debugged? (y or n) y
(gdb) quit
```

## st-flash example
```
[librae@centos Desktop]$ st-flash write build/cube.bin 0x08000000
2014-12-06T00:21:33 INFO src/stlink-common.c: Loading device parameters....
2014-12-06T00:21:33 INFO src/stlink-common.c: Device connected is: F07x device, id 0x20016448
2014-12-06T00:21:33 INFO src/stlink-common.c: SRAM size: 0x4000 bytes (16 KiB), Flash: 0x20000 bytes (128 KiB) in pages of 2048 bytes
2014-12-06T00:21:33 INFO src/stlink-common.c: Attempting to write 38120 (0x94e8) bytes to stm32 address: 134217728 (0x8000000)
Flash page at addr: 0x08009000 erased
2014-12-06T00:21:34 INFO src/stlink-common.c: Finished erasing 19 pages of 2048 (0x800) bytes
2014-12-06T00:21:34 INFO src/stlink-common.c: Starting Flash write for VL/F0/F3 core id
2014-12-06T00:21:34 INFO src/stlink-common.c: Successfully loaded flash loader in sram
18/18 pages written
2014-12-06T00:21:36 INFO src/stlink-common.c: Starting verification of write complete
2014-12-06T00:21:37 INFO src/stlink-common.c: Flash written and verified! jolly good!
```
