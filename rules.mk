# STM32Cube Makefile
# Contact - librae8226@gmail.com

# Useful tools
CROSS   ?= arm-none-eabi-
CC      := $(CROSS)gcc
CXX     := $(CROSS)g++
LD      := $(CROSS)ld
AR      := $(CROSS)ar
AS      := $(CROSS)gcc
GDB     := $(CROSS)gdb
OBJCOPY := $(CROSS)objcopy
DISAS   := $(CROSS)objdump
OBJDUMP := $(CROSS)objdump
SIZE    := $(CROSS)size
NM      := $(CROSS)nm

# Suppress annoying output unless V is set
#define V
ifndef V
	SILENT_CC       = @echo '  [CC]       ' $(@:$(BUILD_PATH)/%.o=%.c);
	SILENT_AS       = @echo '  [AS]       ' $(@:$(BUILD_PATH)/%.o=%.S);
	SILENT_CXX      = @echo '  [CXX]      ' $(@:$(BUILD_PATH)/%.o=%.cpp);
	SILENT_LD       = @echo '  [LD]       ' $(@);
	SILENT_AR       = @echo '  [AR]       ' $(@F);
	SILENT_OBJCOPY  = @echo '  [OBJCOPY]  ' $(@);
	SILENT_DISAS    = @echo '  [DISAS]    ' $(@:%.bin=%).lss;
	SILENT_OBJDUMP  = @echo '  [OBJDUMP]  ' $(OBJDUMP);
	SILENT_NM       = @echo '  [NM]       ' $(@:%.bin=%).sym;
endif
#endef

TGT_BIN :=

CFLAGS   = $(GLOBAL_CFLAGS) $(TGT_CFLAGS)
CXXFLAGS = $(GLOBAL_CXXFLAGS) $(TGT_CXXFLAGS)
ASFLAGS  = $(GLOBAL_ASFLAGS) $(TGT_ASFLAGS)

# General directory independent build rules, generate dependency information
#$(BUILD_PATH)/%.o: %.c
%.o: %.c
	$(SILENT_CC) $(CC) $(CFLAGS) -MMD -MP -MF $(@:%.o=%.d) -MT $@ -o $@ -c $<

#$(BUILD_PATH)/%.o: %.cpp
%.o: %.cpp
	$(SILENT_CXX) $(CXX) $(CFLAGS) $(CXXFLAGS) -MMD -MP -MF $(@:%.o=%.d) -MT $@ -o $@ -c $<

#$(BUILD_PATH)/%.o: %.S
%.o: %.S
	$(SILENT_AS) $(AS) $(ASFLAGS) -MMD -MP -MF $(@:%.o=%.d) -MT $@ -o $@ -c $<
