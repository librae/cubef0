# STM32Cube targets.mk
# Contact - librae8226@gmail.com

#OBJS := $(SSRCS:%.S=$(BUILD_PATH)/%.o) $(CSRCS:%.c=$(BUILD_PATH)/%.o)
#OBJS := $(notdir $(OBJS))
#OBJS := $(addprefix $(BUILD_PATH)/,$(OBJS))
OBJS := $(SSRCS:%.S=%.o) $(CSRCS:%.c=%.o)
DEPS := $(OBJS:%.o=%.d)

-include $(DEPS)

TGT_BIN += $(OBJS)

$(BUILD_PATH)/$(CUBE_TARGET): $(BUILD_PATH)/$(CUBE_TARGET).hex $(BUILD_PATH)/$(CUBE_TARGET).bin

$(BUILD_PATH)/$(CUBE_TARGET).hex: $(BUILD_PATH)/$(CUBE_TARGET).elf
	$(SILENT_OBJCOPY) $(OBJCOPY) -v -O ihex $< $@ 1>/dev/null

$(BUILD_PATH)/$(CUBE_TARGET).bin: $(BUILD_PATH)/$(CUBE_TARGET).elf
	$(SILENT_OBJCOPY) $(OBJCOPY) -v -O binary $< $@ 1>/dev/null
	$(SILENT_DISAS) $(DISAS) -d -x $< > $(BUILD_PATH)/$(CUBE_TARGET).lss
	$(SILENT_NM) $(NM) -n $< > $(BUILD_PATH)/$(CUBE_TARGET).sym
	@find $(CUBE_ROOT) -name \*.o | xargs $(SIZE) -t > $(BUILD_PATH)/$(CUBE_TARGET).sizes
	@echo " "
	@echo "Final Size:"
	@$(SIZE) $<

$(BUILD_PATH)/$(CUBE_TARGET).elf: $(TGT_BIN) $(BUILD_PATH)
	$(SILENT_LD) $(CC) $(LDFLAGS) -o $@ $(TGT_BIN) -Wl,-Map,$(BUILD_PATH)/$(CUBE_TARGET).map

$(BUILD_PATH):
	@mkdir -p $@
