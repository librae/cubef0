# STM32Cube project.mk
# Contact - librae8226@gmail.com

# Outline of Cube Package

#  +-------------+  +---------------+
#  |  USER/APP   |  |               |
#  +-------------+  |   UTILITIES   |
#  +-------------+  | (CMSIS, etc.) |
#  | MIDDLEWARES |  |               |
#  +-------------+  +---------------+
#
#  +--------------------------------+
#  |     HAL       |      BSP       |
#  +--------------------------------+
#
#  +--------------------------------+
#  |           HARDWARE             |
#  +--------------------------------+

CUBE_TARGET = cube

# User Customization Items

PROJ_ROOT    := .
CUBE_ROOT    := $(PROJ_ROOT)/../../../..
BUILD_PATH   := $(PROJ_ROOT)/build
USER_PATH    := $(PROJ_ROOT)/..
BSP_PATH     := $(CUBE_ROOT)/Drivers/BSP
CMSIS_PATH   := $(CUBE_ROOT)/Drivers/CMSIS
HAL_PATH     := $(CUBE_ROOT)/Drivers/STM32F0xx_HAL_Driver
MIDWARE_PATH := $(CUBE_ROOT)/Middlewares
UTILS_PATH   := $(CUBE_ROOT)/Utilities

USER_CSRCS := \
	$(USER_PATH)/Src/main.c \
	$(USER_PATH)/Src/stm32f0xx_hal_msp.c \
	$(USER_PATH)/Src/stm32f0xx_it.c \
	$(USER_PATH)/Src/tsl_user.c \
	$(USER_PATH)/Src/usbd_conf.c \
	$(USER_PATH)/Src/usbd_desc.c

MIDWARE_CSRCS := \
	$(MIDWARE_PATH)/ST/STM32_TouchSensing_Library/src/tsl.c \
	$(MIDWARE_PATH)/ST/STM32_TouchSensing_Library/src/tsl_acq.c \
	$(MIDWARE_PATH)/ST/STM32_TouchSensing_Library/src/tsl_acq_tsc.c \
	$(MIDWARE_PATH)/ST/STM32_TouchSensing_Library/src/tsl_dxs.c \
	$(MIDWARE_PATH)/ST/STM32_TouchSensing_Library/src/tsl_ecs.c \
	$(MIDWARE_PATH)/ST/STM32_TouchSensing_Library/src/tsl_filter.c \
	$(MIDWARE_PATH)/ST/STM32_TouchSensing_Library/src/tsl_globals.c \
	$(MIDWARE_PATH)/ST/STM32_TouchSensing_Library/src/tsl_linrot.c \
	$(MIDWARE_PATH)/ST/STM32_TouchSensing_Library/src/tsl_object.c \
	$(MIDWARE_PATH)/ST/STM32_TouchSensing_Library/src/tsl_time.c \
	$(MIDWARE_PATH)/ST/STM32_TouchSensing_Library/src/tsl_touchkey.c \
	$(MIDWARE_PATH)/ST/STM32_USB_Device_Library/Class/HID/Src/usbd_hid.c \
	$(MIDWARE_PATH)/ST/STM32_USB_Device_Library/Core/Src/usbd_core.c \
	$(MIDWARE_PATH)/ST/STM32_USB_Device_Library/Core/Src/usbd_ctlreq.c \
	$(MIDWARE_PATH)/ST/STM32_USB_Device_Library/Core/Src/usbd_ioreq.c

CMSIS_CSRCS := \
	$(USER_PATH)/Src/system_stm32f0xx.c

BSP_CSRCS := \
	$(BSP_PATH)/Components/l3gd20/l3gd20.c \
	$(BSP_PATH)/STM32F072B-Discovery/stm32f072b_discovery.c \
	$(BSP_PATH)/STM32F072B-Discovery/stm32f072b_discovery_gyroscope.c

HAL_CSRCS := \
	$(HAL_PATH)/Src/stm32f0xx_hal.c \
	$(HAL_PATH)/Src/stm32f0xx_hal_cortex.c \
	$(HAL_PATH)/Src/stm32f0xx_hal_dma.c \
	$(HAL_PATH)/Src/stm32f0xx_hal_flash.c \
	$(HAL_PATH)/Src/stm32f0xx_hal_flash_ex.c \
	$(HAL_PATH)/Src/stm32f0xx_hal_gpio.c \
	$(HAL_PATH)/Src/stm32f0xx_hal_pcd.c \
	$(HAL_PATH)/Src/stm32f0xx_hal_pcd_ex.c \
	$(HAL_PATH)/Src/stm32f0xx_hal_pwr.c \
	$(HAL_PATH)/Src/stm32f0xx_hal_pwr_ex.c \
	$(HAL_PATH)/Src/stm32f0xx_hal_rcc.c \
	$(HAL_PATH)/Src/stm32f0xx_hal_rcc_ex.c \
	$(HAL_PATH)/Src/stm32f0xx_hal_spi.c \
	$(HAL_PATH)/Src/stm32f0xx_hal_tsc.c \
	$(HAL_PATH)/Src/stm32f0xx_hal_uart.c \
	$(HAL_PATH)/Src/stm32f0xx_hal_uart_ex.c

SSRCS := $(PROJ_ROOT)/startup_stm32f072xb.S

CSRCS := $(HAL_CSRCS) $(BSP_CSRCS) $(CMSIS_CSRCS) $(MIDWARE_CSRCS) $(USER_CSRCS)

USER_INCLUDES := \
	-I$(USER_PATH)/Inc

INCLUDES := \
	-I$(CMSIS_PATH)/Include \
	-I$(CMSIS_PATH)/Device/ST/STM32F0xx/Include \
	-I$(HAL_PATH)/Inc \
	-I$(BSP_PATH)/STM32F072B-Discovery \
	-I$(BSP_PATH)/Components/Common \
	-I$(MIDWARE_PATH)/ST/STM32_TouchSensing_Library/inc \
	-I$(MIDWARE_PATH)/ST/STM32_USB_Device_Library/Core/Inc \
	-I$(MIDWARE_PATH)/ST/STM32_USB_Device_Library/Class/HID/Inc \
	-I$(UTILS_PATH)/Log \
	-I$(UTILS_PATH)/CPU

GLOBAL_DEFS := \
	-DSTM32F072xB \
	-DUSE_HAL_DRIVER \
	-DUSE_STM32F072B_DISCO

GLOBAL_CFLAGS := \
	-ggdb -g3 -std=gnu99 -O2 -Wall -Wstrict-prototypes \
	-mcpu=cortex-m0 -mlittle-endian -mthumb -mthumb-interwork \
	-ffunction-sections -fdata-sections -fno-builtin \
	$(INCLUDES) $(USER_INCLUDES) $(GLOBAL_DEFS)

GLOBAL_ASFLAGS := \
	-ggdb -g3 -mcpu=cortex-m0 -mlittle-endian -mthumb

LINKERSCRIPT := $(PROJ_ROOT)/flash.ld

LDFLAGS := \
	-g3 -ggdb -std=gnu99 -mlittle-endian -static -lgcc -mthumb \
	-mthumb-interwork -specs=nano.specs -lc -lnosys -Xlinker \
	-T$(LINKERSCRIPT) -mcpu=cortex-m0
