# STM32Cube Makefile
# Contact - librae8226@gmail.com

include project.mk

.DEFAULT_GOAL := sketch

include rules.mk
include targets.mk

.PHONY: all sketch upload debug info clean distclean help

all: sketch
sketch: $(BUILD_PATH)/$(CUBE_TARGET)

# Currently support stlink tools only, including:
#   st-flash, st-info, st-term, st-util
# Refer to url:
#   https://github.com/texane/stlink

upload: $(BUILD_PATH)/$(CUBE_TARGET).bin
	@st-flash write $< 0x08000000

debug: $(BUILD_PATH)/$(CUBE_TARGET).elf
	@echo "Starting gdb server..."
	@echo "Please run a gdb client, e.g."
	@echo "   " $(GDB) $<
	@echo "And then try to connect default target:"
	@echo "    (gdb) tar ext :4242"
	@st-util

# Note!
# The dfu functionality needs support by bootloader.
# Different bootloaders may need different parameters applied to dfu-util.
# Below command is default VID:PID to 0483:3748.
# If not sure, please ignore the dfu function.
dfu: $(BUILD_PATH)/$(CUBE_TARGET).bin
	@dfu-util -a1 -d 0483:3748 -D $< -R

info:
	@echo "Flash:"
	@st-info --flash
	@echo "Sram:"
	@st-info --sram
	@echo "Descr:"
	@st-info --descr
	@echo "Page size:"
	@st-info --pagesize
	@echo "Chip ID:"
	@st-info --chipid

clean:
	$(shell rm -rf $(OBJS) $(DEPS))
	$(shell rm -rf $(BUILD_PATH))

distclean: clean
	$(shell rm -f tags tags.ut tags.fn cscope.*)
	$(shell find . -name "*.swp" -exec rm -rf {} \;)
	@echo cleaned up completely

help:
	@echo ""
	@echo "==========================================================="
	@echo "[STM32Cube Make Help]"
	@echo ""
	@echo "Building targets:"
	@echo "  all:        build all of the sketch"
	@echo "  sketch:     default build"
	@echo ""
	@echo "Firmware specific targets:"
	@echo "  upload:     upload firmware image to the chip with st-flash"
	@echo "  dfu:        upload firmware with dfu-util, bootloader required"
	@echo "  info:       read chip information with st-info"
	@echo "  debug:      start a gdb server with st-util"
	@echo "  (dependency - https://github.com/texane/stlink.git)"
	@echo ""
	@echo "Other targets:"
	@echo "  clean:      remove all build files"
	@echo "  distclean:  remove all builds tarballs, and other misc"
	@echo "  help:       show this message"
	@echo "==========================================================="
	@echo ""
